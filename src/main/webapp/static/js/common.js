// ------------ jQuery ------------- //

$.postJSON = function(url, data, callback) {
    return $.ajax({
        contentType : 'application/json',
        type : 'POST',
        url : url,
        data : JSON.stringify(data),
        dataType : 'json',
        success : callback
    });
};
