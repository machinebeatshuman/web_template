package web_template.service.hackgrowth;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

/**
 * Hacks click actions.
 * 
 * @author wangxiao
 */
@Service
public class ClickHacker {

    @PostConstruct
    public void init() {
        new Timer("ClickHacker").schedule(new TimerTask() {
            @Override
            public void run() {
                // TODO
            }
        }, 0, HACK_INTERVAL_MS);
    }

    // @Autowired

    private static final int HACK_INTERVAL_MS = 24 * 3600 * 1000; // Everyday.

}
