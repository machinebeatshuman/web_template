package web_template.service.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.apachecommons.CommonsLog;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import web_template.dao.IConfigDao;
import web_template.datamodel.Config;

/**
 * Reads configuration from database.
 * 
 * @author wangxiao
 */
@CommonsLog
@Component
public class DatabaseConfig implements IConfig {

    @Override
    public String getVariable(String name) {
        return buffer.get(name);
    }

    public void update() {
        log.info("Update database config");

        Map<String, String> tmp = new HashMap<String, String>();
        List<Config> configs = configDao.getAll(Config.class);
        for (Config config : configs) {
            tmp.put(config.getKey(), config.getValue());
        }

        log.info("Database config updated: size " + buffer.size() + " -> "
                + tmp.size());
        buffer = tmp;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    private Map<String, String> buffer = new HashMap<String, String>();

    @Autowired
    private IConfigDao configDao;

}
