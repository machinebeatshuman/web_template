package web_template.service.config;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.map.LRUMap;

/**
 * Wrapper of a given {@link IConfig} instance to cache variables with specified
 * TTL. Each variable will have its own expiration detection.
 * 
 * Thread-safe.
 * 
 * @author wangxiao
 */
@RequiredArgsConstructor
public class CachedConfig implements IConfig {

    @Override
    public synchronized String getVariable(String name) {
        if (name == null) {
            return null;
        }

        CacheEntry cacheEntry = cache.get(name);
        if (cacheEntry == null) { // null value is also cached
            cache.put(name, cacheEntry = new CacheEntry());
            cacheEntry.update(config.getVariable(name), ttl);
        } else if (cacheEntry.expireTime.compareTo(new Date()) < 0) { // expired
            cacheEntry.update(config.getVariable(name), ttl);
        }
        return cacheEntry.value;
    }

    /**
     * Clears cache and updates wrapped config.
     */
    @Override
    public synchronized void update() {
        config.update();
        cache.clear();
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " of " + config;
    }

    private final static int CACHE_SIZE = 100000;

    /**
     * The wrapped {@link IConfig} instance.
     */
    @NonNull
    private IConfig config;

    @SuppressWarnings("unchecked")
    private Map<String, CacheEntry> cache = MapUtils
            .synchronizedMap(new LRUMap(CACHE_SIZE));

    /**
     * TTL in millisecond for each variable.
     */
    @Setter
    @Getter
    private int ttl = 1000;

    /**
     * Value of cache.
     * 
     * @author wangxiao
     */
    @ToString
    private static class CacheEntry {
        public void update(String value, int ttl) {
            this.value = value;
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.MILLISECOND, ttl);
            expireTime = calendar.getTime();
        }

        private String value;
        private Date expireTime;
    }

}
