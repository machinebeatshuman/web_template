package web_template.service.config;

/**
 * Interface for generic configuration.
 * 
 * @author wangxiao
 */
public interface IConfig {

    /**
     * @param name
     *            variable name (e.g. "foo.bar"), case sensitive
     * @return variable value or null
     */
    String getVariable(String name);

    /**
     * Updates the configuration, e.g. forces reloading data.
     */
    public void update();

}
