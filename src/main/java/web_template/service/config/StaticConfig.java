package web_template.service.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import lombok.extern.apachecommons.CommonsLog;

/**
 * The configuration for predefined static variables.
 * 
 * Thread-safe.
 * 
 * @author wangxiao
 */
@CommonsLog
public class StaticConfig implements IConfig {

    public StaticConfig() {
        // Prints all predefined variables.
        log.info("Predefined variables:");
        for (Entry<String, String> entry : variableMap.entrySet()) {
            log.info(entry.getKey() + ": " + entry.getValue());
        }
    }

    /**
     * Returns variable name as value if not found.
     */
    @Override
    public String getVariable(String name) {
        String result = variableMap.get(name);
        return result == null ? name : result;
    }

    @Override
    public void update() {
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    @SuppressWarnings("serial")
    private Map<String, String> variableMap = new HashMap<String, String>() {
        {
            // Adds predefined variables here:
            put("predefined_name", "predefined_value");
        }
    };

}