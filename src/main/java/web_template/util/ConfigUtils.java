package web_template.util;

import java.util.Timer;
import java.util.TimerTask;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Component;

import web_template.service.config.CachedConfig;
import web_template.service.config.IConfig;
import web_template.service.config.StaticConfig;

/**
 * Utility for configuration.
 * 
 * Thread-safe.
 * 
 * @author wangxiao
 */
@CommonsLog
@Component
public class ConfigUtils {

    /**
     * Injects database configuration, which has the highest priority against
     * others.
     */
    public static void setConfigs(final IConfig databaseConfig) {
        configs = (IConfig[]) ArrayUtils.add(configs, 0, new CachedConfig(
                databaseConfig));

        log.info("Configs:");
        for (IConfig config : configs) {
            log.info(config);
        }

        new Timer("ConfigUpdater").schedule(new TimerTask() {
            @Override
            public void run() {
                log.info("Update configs:");
                for (IConfig config : configs) {
                    try {
                        log.info("Update config: " + config);
                        config.update();
                    } catch (Exception e) {
                        log.error("Update config failed: " + config, e);
                    }
                }
            }
        }, 0, UPDATE_INTERVAL_MS);
    }

    /**
     * @param name
     *            variable name, following the format
     *            "namespace1.namespace2...variableName"
     * @return variable value of the first hit configuration, or null if not
     *         found
     */
    public static String getVariable(String name) {
        for (IConfig config : configs) {
            String value = config.getVariable(name);
            if (value != null) {
                return value;
            }
        }
        return null;
    }

    /**
     * NOTE: the DefaultConfig should be the last {@link IConfig} instance in
     * case of overwriting other configuration.
     * 
     * TODO add real config
     */
    private static IConfig[] configs = { new StaticConfig() };

    /**
     * Configuration update interval: 5 minutes.
     */
    private static final int UPDATE_INTERVAL_MS = 5 * 60 * 1000;

}
