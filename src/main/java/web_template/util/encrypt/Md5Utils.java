package web_template.util.encrypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.lang.Validate;

/**
 * Thread-safe.
 * 
 * @author WangXiao
 */
public class Md5Utils {

    /**
     * Thread local instance.
     */
    private static final ThreadLocal<MessageDigest> LOCAL_MD5 = new ThreadLocal<MessageDigest>() {
        @Override
        protected MessageDigest initialValue() {
            try {
                return MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    };

    /**
     * @param bytes
     *            non-null
     * @param offset
     *            see {@link MessageDigest#update(byte[], int, int)}
     * @param len
     *            see {@link MessageDigest#update(byte[], int, int)}
     * @return 128 bits MD5 result
     */
    public static byte[] sign128bit(byte[] bytes, int offset, int len) {
        Validate.notNull(bytes);
        MessageDigest md = LOCAL_MD5.get();
        md.update(bytes, offset, len);
        return md.digest();
    }

    /**
     * @param bytes
     *            non-null
     * @return 128 bits MD5 result
     */
    public static byte[] sign128bit(byte[] bytes) {
        Validate.notNull(bytes);
        MessageDigest md = LOCAL_MD5.get();
        return md.digest(bytes);
    }

    /**
     * @param bytes
     *            non-null
     * @return 64 bits MD5 result
     */
    public static long sign64bit(byte[] bytes) {
        Validate.notNull(bytes);
        MessageDigest md = LOCAL_MD5.get();
        byte[] result = md.digest(bytes);
        long code = (((long) result[7]) << 56) + (((long) result[6]) << 48)
                + (((long) result[5]) << 40) + (((long) result[4]) << 32)
                + (((long) result[3]) << 24) + (((long) result[2]) << 16)
                + (((long) result[1]) << 8) + (((long) result[0]));
        return code;
    }

    /**
     * @param bytes
     *            non-null
     * @return 32 bits MD5 result
     */
    public static int sign32bit(byte[] bytes) {
        Validate.notNull(bytes);
        MessageDigest md = LOCAL_MD5.get();
        byte[] result = md.digest(bytes);
        int code = (((int) result[3]) << 24) + (((int) result[2]) << 16)
                + (((int) result[1]) << 8) + (((int) result[0]));
        return code;
    }

}
