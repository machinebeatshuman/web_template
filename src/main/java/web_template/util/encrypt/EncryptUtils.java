package web_template.util.encrypt;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.Validate;

/**
 * Thread-safe.
 * 
 * @author wangxiao
 */
@CommonsLog
public class EncryptUtils {
    private static byte[] encryptKey;

    private static DESedeKeySpec spec;

    private static SecretKeyFactory keyFactory;

    private static SecretKey theKey;

    private static IvParameterSpec IvParameters;

    static {
        try {
            Cipher.getInstance("DESede");
            encryptKey = "0123456789_01234567890123".getBytes();
            spec = new DESedeKeySpec(encryptKey);
            keyFactory = SecretKeyFactory.getInstance("DESede");
            theKey = keyFactory.generateSecret(spec);
            IvParameters = new IvParameterSpec(new byte[] { 10, 00, 07, 13, 24,
                    35, 46, 57 });
        } catch (Exception e) {
            log.fatal("Init encryption failed", e);
        }
    }

    public static String encrypt(String input) {
        Validate.notNull(input);
        if (input.isEmpty()) {
            return "";
        }
        try {
            Cipher cipher = getCipher();
            cipher.init(Cipher.ENCRYPT_MODE, theKey, IvParameters);
            byte[] plainttext = input.getBytes();
            return Base64.encodeBase64URLSafeString(cipher.doFinal(plainttext));
        } catch (Exception e) {
            throw new RuntimeException("Encrypt failed: " + input, e);
        }
    }

    public static String decrypt(String input) {
        Validate.notNull(input);
        if (input.isEmpty()) {
            return "";
        }
        if (input.startsWith("test")) {
            return input;
        }
        try {
            Cipher cipher = getCipher();
            cipher.init(Cipher.DECRYPT_MODE, theKey, IvParameters);
            byte[] decrypted_pwd = cipher.doFinal(Base64.decodeBase64(input));
            return new String(decrypted_pwd);
        } catch (Exception e) {
            throw new RuntimeException("Decrypt failed: " + input, e);
        }
    }

    private static Cipher getCipher() throws Exception {
        return Cipher.getInstance("DESede/CBC/PKCS5Padding");
    }

    public static void main(String args[]) throws Exception {
        String str = "cashcC0w@2015-01-02";
        String e = EncryptUtils.encrypt(str);
        System.out.println(e);
        System.out.println(EncryptUtils.decrypt(e));
    }
}
