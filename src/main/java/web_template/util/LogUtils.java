package web_template.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import web_template.util.user.CookieUtils;
import web_template.util.user.UserUtils;

/**
 * Log user behaviors. Example:
 * 
 * <pre>
 * UserBehaviorLog.XXX.info(&quot;Action name: username=&quot; + username + &quot;, cookieId=&quot;
 *         + cookieId + &quot;, xxx=&quot; + xxx);
 * </pre>
 * 
 * @author wangxiao
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LogUtils {

    /**
     * Administrator actions.
     */
    public static Log ADMIN = getLog("admin");

    /**
     * User actions.
     */
    public static Log USER = getLog("user");

    /**
     * Authentication.
     */
    public static Log AUTH = getLog("auth");

    /**
     * Statistics.
     */
    public static Log STAT = getLog("STAT");

    /**
     * @return User's signature for diagnosis.
     */
    public static String getUserSignature(HttpServletRequest request,
            HttpServletResponse response) {
        return "username=" + UserUtils.getUsername(request) + ", cookieId="
                + CookieUtils.getCookieId(request, response);
    }

    private static Log getLog(String logName) {
        return LogFactory.getLog("web_template.log." + logName);
    }

}
