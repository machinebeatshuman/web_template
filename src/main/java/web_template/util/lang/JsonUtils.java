package web_template.util.lang;

import java.util.HashMap;
import java.util.Map;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.lang.StringUtils;

import web_template.util.encrypt.EncryptUtils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author wangxiao
 */
@CommonsLog
public class JsonUtils {

    public static String toJson(Object o) {
        return GSON.toJson(o);
    }

    public static String toJson(Object o, ExclusionStrategy strategy) {
        try {
            GsonBuilder builder = new GsonBuilder();
            if (strategy != null) {
                builder.addSerializationExclusionStrategy(strategy);
            }
            return builder.create().toJson(o);
        } catch (Exception e) {
            log.error("To JSON failed: object=" + o);
            return "";
        }
    }

    public static String toJson(String key, Object value) {
        key = StringUtils.trimToNull(key);
        if (key == null) {
            return null;
        }
        Map<Object, Object> map = new HashMap<Object, Object>();
        map.put(key, value);
        return GSON.toJson(map);
    }

    /**
     * @return data from encrypted JSON
     */
    public static <T> T decrypt(String encryptedJson, Class<T> dataClass)
            throws Exception {
        encryptedJson = StringUtils.trimToNull(encryptedJson);
        if (encryptedJson == null) {
            return null;
        }
        String json = EncryptUtils.decrypt(encryptedJson);
        return GSON.fromJson(json, dataClass);
    }

    /**
     * @return encrypted JSON of given data
     */
    public static <T> String encrypt(T data) throws Exception {
        if (data == null) {
            return null;
        }
        return EncryptUtils.encrypt(GSON.toJson(data));
    }

    public static final Gson GSON = new GsonBuilder().disableHtmlEscaping()
            .create();

}
