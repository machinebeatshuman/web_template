package web_template.util.lang;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

/**
 * @author wangxiao
 */
@CommonsLog
public class StringUtils extends org.apache.commons.lang.StringUtils {

    /**
     * @return like '1|2|3'
     */
    public static String toString(Object... o) {
        return org.apache.commons.lang.StringUtils.join(o, '|');
    }

    public static String escpaeHtml(String text) {
        if (text == null) {
            return null;
        }
        StringBuilder builder = new StringBuilder();
        text = StringEscapeUtils.escapeHtml(text);
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char c = text.charAt(i);
            if (c == '\r' && i < length - 1 && text.charAt(i + 1) == '\n') { // \r\n
                builder.append("<br>");
                i++;
            } else if (c == '\r' || c == '\n') {
                builder.append("<br>");
            } else {
                builder.append(c);
            }
        }
        return builder.toString();
    }

    /**
     * Sanitizes HTML by removing harmful elements, attributes etc.
     */
    public static String sanitizeHtml(String html) {
        if (StringUtils.isEmpty(html)) {
            return null;
        }
        return Jsoup.clean(html, SANITIZE_HTML_WHITELIST);
    }

    /**
     * Abstract and then escape to JS.
     */
    public static String escapeSummary(String text, int maxLength) {
        if (text == null) {
            return null;
        }
        text = abbreviate(text, maxLength);
        return StringEscapeUtils.escapeJavaScript(text);
    }

    public static String escapeJS(String text) {
        return StringEscapeUtils.escapeJavaScript(text);
    }

    /**
     * Encodes (lower-case, without whitespace) text as a readable part in URL.
     * e.g. "a B     c" to "a-b-c".
     * 
     * @return Empty string if empty text.
     */
    public static String encodeReadableStringUrlComponent(String text) {
        text = StringUtils.trimToNull(text);
        if (text == null) {
            return "";
        }
        String beforeEncode = text.toLowerCase().replaceAll("\\s+", "-");
        try {
            return URLEncoder.encode(beforeEncode, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            log.error("URL encode failed: " + beforeEncode);
            return beforeEncode;
        }
    }

    private static final Whitelist SANITIZE_HTML_WHITELIST = Whitelist
            .relaxed().removeTags("img");

}
