package web_template.util.lang;

/**
 * @author wangxiao
 */
public interface IJsonizable {

    /**
     * @return non-null
     */
    String toJson();

}
