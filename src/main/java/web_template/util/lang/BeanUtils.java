package web_template.util.lang;

import java.lang.reflect.Field;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.lang.Validate;

/**
 * @author wangxiao
 */
@CommonsLog
public class BeanUtils extends org.apache.commons.beanutils.BeanUtils {

    /**
     * Like HTTP PATCH method, patches non-null fields from one object to
     * another.
     * 
     * @param from
     *            non-null
     * @param to
     *            non-null
     */
    public static void patchObject(Object from, Object to) {
        Validate.notNull(from);
        Validate.notNull(to);
        Validate.isTrue(from.getClass() == to.getClass());

        for (Field field : from.getClass().getDeclaredFields()) {
            Object fromValue = null;
            try {
                field.setAccessible(true);
                fromValue = field.get(from);
            } catch (Exception e) {
                log.warn("Failed to get value from object with reflection", e);
            }
            if (fromValue == null) {
                continue;
            }

            try {
                field.set(to, fromValue);
            } catch (Exception e) {
                log.warn("Failed to set value to object with reflection", e);
            }
        }
    }

}
