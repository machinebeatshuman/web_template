package web_template.util.web;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;

import web_template.web.session.SessionKeys;

/**
 * @author wangxiao
 */
public class AdminUtils {

    /**
     * @return True is has logged in.
     */
    public static boolean isLoggedIn(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("Null request");
        }
        return WebUtils.getSessionValue(request, SessionKeys.ADMIN) != null;
    }

    /**
     * @param username
     *            The use name of admin, non-null.
     * @return True if successful to log in.
     */
    public static boolean logIn(HttpServletRequest request, String username,
            String password) {
        if (request == null) {
            throw new IllegalArgumentException("Null request");
        }

        if (StringUtils.equals(username, ".")
                && StringUtils.equals(password, ".")) {
            request.getSession().setAttribute(SessionKeys.ADMIN, username);
            return true;
        }
        return false;
    }

}
