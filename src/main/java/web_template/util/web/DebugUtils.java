package web_template.util.web;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.servlet.http.HttpServletRequest;

/**
 * @author wangxiao
 */
public class DebugUtils {

    /**
     * With "true" value when in debug model.
     */
    public static final String DEBUG_PARAM = "debugx";

    private static final ThreadLocal<AtomicBoolean> DEBUG_ENABLED = new ThreadLocal<AtomicBoolean>() {
        @Override
        protected AtomicBoolean initialValue() {
            return new AtomicBoolean();
        }
    };

    /**
     * @return whether debug enabled for the specified request
     */
    public static boolean isDebugEnabled(HttpServletRequest request) {
        return request.getParameter(DEBUG_PARAM) != null;
    }

    /**
     * "debug=detail" to show details on page.
     */
    public static boolean isShowDebugDetail(HttpServletRequest request) {
        return "1".equals(request.getParameter(DebugUtils.DEBUG_PARAM));
    }

    /**
     * @return whether debug enabled currently
     */
    public static boolean isDebugEnabled() {
        return DEBUG_ENABLED.get().get();
    }

    /**
     * Update the debug status.
     */
    public static void setDebugEnabled(boolean debugEnabled) {
        DEBUG_ENABLED.get().set(debugEnabled);
    }

    /**
     * @param url
     *            usually parameter of #spiringUrl()
     * @return URL with wrap of "debug=0" when needed
     */
    public static String wrapDebug(String url) {
        if (url == null || !isDebugEnabled()) {
            return url;
        }
        if (url.startsWith("/static")) {
            return url;
        }
        if (url.contains("?")) {
            url += "&debug=0";
        } else {
            if (!url.endsWith("/")) {
                url += "/";
            }
            url += "?debug=0";
        }
        return url;
    }

}
