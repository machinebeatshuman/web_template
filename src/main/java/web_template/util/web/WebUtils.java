package web_template.util.web;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.Validate;

import web_template.util.lang.JsonUtils;
import web_template.util.lang.StringUtils;

import com.google.gson.JsonSyntaxException;

/**
 * @author wangxiao
 */
@CommonsLog
public class WebUtils {

    private static final int DEFAULT_TIMEOUT = 5000; // 5 seconds

    /**
     * @param request
     *            assert non-null
     */
    public static <T> T getSessionValue(HttpServletRequest request, String key) {
        return getSessionValue(request, key, null);
    }

    /**
     * @param request
     *            assert non-null
     */
    @SuppressWarnings("unchecked")
    public static <T> T getSessionValue(HttpServletRequest request, String key,
            T defaultValue) {
        HttpSession session = request.getSession(false);
        T result = session == null ? null : (T) session.getAttribute(key);
        return result == null ? defaultValue : result;
    }

    public static void invalidateSession(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }

    public static Boolean getBoolean(HttpServletRequest request, String param,
            Boolean defaultValue) {
        try {
            return !request.getParameter(param).equals("0");
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static Integer getInt(HttpServletRequest request, String param,
            Integer defaultValue) {
        try {
            return Integer.parseInt(request.getParameter(param));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static String getNullIfEmpty(HttpServletRequest request, String param) {
        return StringUtils.trimToNull(request.getParameter(param));
    }

    public static <T> T getJson(HttpServletRequest request, String param,
            Class<T> valueClass, T defaultValue) {
        String v = request.getParameter(param);
        if (v == null) {
            return defaultValue;
        }
        try {
            return JsonUtils.GSON.fromJson(v, valueClass);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static String postJson(String url, String content) throws Exception {
        return postJson(url, content, DEFAULT_TIMEOUT);
    }

    public static String postJson(String url, String content, int timeout)
            throws Exception {
        Validate.notNull(url);
        if (log.isDebugEnabled()) {
            log.debug("POST: url=" + url + ", content=" + content);
        }
        HttpURLConnection connection = (HttpURLConnection) new URL(url)
                .openConnection();
        try {
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            // write
            if (content != null) {
                OutputStreamWriter out = new OutputStreamWriter(
                        connection.getOutputStream(), "UTF-8");
                try {
                    out.write(content);
                } finally {
                    IOUtils.closeQuietly(out);
                }
            }
            // read
            return IOUtils.toString(connection.getInputStream());
        } finally {
            IOUtils.close(connection);
        }
    }

    /**
     * Gets JSON object from the response of the specified URL.
     * 
     * @return non-null is not guaranteed
     */
    public static <T> T getJson(String url, Class<T> jsonClass)
            throws Exception {
        Validate.notNull(url);
        Validate.notNull(jsonClass);
        if (log.isDebugEnabled()) {
            log.debug(url);
        }
        URLConnection connection = new URL(url).openConnection();
        try {
            connection.setConnectTimeout(DEFAULT_TIMEOUT);
            connection.setReadTimeout(DEFAULT_TIMEOUT);
            String response = IOUtils.toString(connection.getInputStream(),
                    "UTF-8");
            try {
                T result = JsonUtils.GSON.fromJson(response, jsonClass);
                if (log.isDebugEnabled()) {
                    log.debug(result);
                }
                return result;
            } catch (JsonSyntaxException e) {
                log.error("Invalid JSON: " + response);
                throw e;
            }
        }

        finally {
            IOUtils.close(connection);
        }
    }

    /**
     * Solves the real-IP problem when the server receives request forwarded by
     * local proxy.
     */
    public static String getRealIp(HttpServletRequest request) {
        String result = request.getRemoteAddr();
        if ("127.0.0.1".equals(result)) {
            String s = request.getHeader("X-Real-IP");
            if (s != null) {
                result = s;
            }
        }
        return result;
    }

    /**
     * @return null when unknown
     */
    public static String getBrowser(HttpServletRequest request) {
        String userAgent = request.getHeader("User-Agent");
        if (userAgent != null) {
            userAgent = userAgent.toLowerCase();
            if (userAgent.contains("msie")) {
                return "ie";
            } else if (userAgent.contains("firefox")) {
                return "firefox";
            } else if (userAgent.contains("chrome")) {
                return "chrome";
            }
        }
        return null;
    }

}
