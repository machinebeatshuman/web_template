package web_template.util.web;

import javax.servlet.http.HttpServletResponse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import web_template.util.lang.IJsonizable;
import web_template.util.lang.JsonUtils;

/**
 * @author wangxiao
 */
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class JsonResponse implements IJsonizable {

    public static final JsonResponse OK = new JsonResponse(
            HttpServletResponse.SC_OK);

    public static final JsonResponse NO_CONTENT = new JsonResponse(
            HttpServletResponse.SC_NO_CONTENT);

    public static final JsonResponse NOT_LOGGED_IN = new JsonResponse(
            HttpServletResponse.SC_UNAUTHORIZED, "not logged in");

    public static final JsonResponse NOT_FOUND = new JsonResponse(
            HttpServletResponse.SC_NOT_FOUND);

    @Setter
    @Getter
    @NonNull
    private Integer status;

    @Setter
    @Getter
    private String message;

    public JsonResponse(String message) {
        status = HttpServletResponse.SC_OK;
        this.message = message;
    }

    @Override
    public String toJson() {
        return JsonUtils.toJson(this);
    }

}
