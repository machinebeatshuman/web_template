package web_template.util.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.lang.Validate;

import web_template.datamodel.User;
import web_template.util.LogUtils;
import web_template.util.encrypt.Md5Utils;
import web_template.util.web.WebUtils;

/**
 * @author wangxiao
 */
@CommonsLog
public class UserUtils {

    public static String getUsername(HttpServletRequest request) {
        User user = getUser(request);
        return user == null ? null : user.getUsername();
    }

    public static User getUser(HttpServletRequest request) {
        return WebUtils.getSessionValue(request, User.class.getSimpleName());
    }

    /**
     * Update session to logged-in status.
     * 
     * @param user
     *            user to login, loaded from DB
     */
    public static void login(User user, HttpServletRequest request,
            HttpServletResponse response) {
        if (user == null) {
            log.error("Login failed: null user, request="
                    + request.getRequestURI());
            return;
        }

        // session
        HttpSession session = request.getSession();
        session.setAttribute(User.class.getSimpleName(), user);

        // cookie
        CookieUtils
                .setCookie(response, CookieKeys.USERNAME, user.getUsername());
        LogUtils.USER.info("Login: username=" + user.getUsername()
                + ", cookieId=" + CookieUtils.getCookieId(request, response)
                + ", IP=" + WebUtils.getRealIp(request));
    }

    /**
     * Update user in session.
     */
    public static void update(User user, HttpServletRequest request) {
        request.getSession().setAttribute(User.class.getSimpleName(), user);
    }

    /**
     * @param password
     *            non-null
     */
    public static String encryptPassword(String password) {
        Validate.notNull(password);
        return Long.toString(Md5Utils.sign64bit(password.getBytes()), 16);
    }

}
