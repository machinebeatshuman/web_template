package web_template.util.user;

/**
 * Cookie keys.
 * 
 * @author wangxiao
 */
public interface CookieKeys {

    /**
     * The ID stored in cookie to specify (not precisely) a user.
     */
    String USER_COOKIE_ID = "cid";

    /**
     * The ID stored in cookie to specify (not precisely) a user.
     */
    String USERNAME = "uid";

}
