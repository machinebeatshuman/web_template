package web_template.util.user;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.apachecommons.CommonsLog;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang.math.RandomUtils;

import web_template.util.encrypt.Md5Utils;

/**
 * @author wangxiao
 */
@CommonsLog
public class CookieUtils {

    public static Cookie setCookie(HttpServletResponse response, String key,
            String value) {
        Validate.notNull(key);

        Cookie result = new Cookie(key, value);
        result.setPath("/"); // global
        result.setMaxAge(TTL_INFINITE);
        if (log.isDebugEnabled()) {
            log.debug("Set cookie: key=" + key + ", value=" + value);
        }
        response.addHeader("P3P", "CP=CAO PSA OUR"); // fix for ie6/ie7
        response.addCookie(result);
        return result;
    }

    public static Cookie getCookie(HttpServletRequest request, String key) {
        Validate.notNull(key);

        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (key.equals(c.getName())) {
                    return c;
                }
            }
        }
        return null;
    }

    public static String getCookieValue(HttpServletRequest request, String key) {
        Cookie cookie = getCookie(request, key);
        return cookie == null ? null : cookie.getValue();
    }

    /**
     * @return Cookie ID if it exists, or else creates a new cookie ID and
     *         stores it in cookie.
     */
    public static String getCookieId(HttpServletRequest request,
            HttpServletResponse response) {
        Validate.notNull(request);
        Validate.notNull(response);
        Cookie cookie = getCookie(request, CookieKeys.USER_COOKIE_ID);

        // create if not exist
        if (cookie == null) {
            String cookieSeed = RandomUtils.nextInt() + request.getRemoteAddr()
                    + System.currentTimeMillis();
            long cookieId = Math.abs(Md5Utils.sign64bit(cookieSeed.getBytes()));
            cookie = setCookie(response, CookieKeys.USER_COOKIE_ID,
                    Long.toString(cookieId));
        }
        return cookie.getValue();
    }

    private static final int TTL_INFINITE = 3600 * 24 * 3650; // 10 years

}
