/**
 * AOP layer to decouple business unrelated logics, e.g. logging, authentication etc.
 */
package web_template.aop;