package web_template.aop;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import web_template.util.web.AdminUtils;

/**
 * @author wangxiao
 */
public class AdminInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
        if (request.getRequestURI().contains("/adminx/login")) {
            return super.preHandle(request, response, handler);
        }
        // validate
        if (!AdminUtils.isLoggedIn(request)) {
            response.sendRedirect("/adminx/login");
            return false;
        }
        return true;
    }

}
