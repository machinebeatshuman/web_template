package web_template.datamodel.schema;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * See {@link DatabaseField#STATUS}.
 * 
 * @author wangxiao
 */
@AllArgsConstructor
public enum Status {

    OK(0),

    DELETED(-1);

    @Getter
    private int value;

}
