package web_template.datamodel.schema;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The fields used in most tables.
 * 
 * @author wangxiao
 */
@AllArgsConstructor
public enum DatabaseField {

    ID("id"),

    STATUS("status"),

    CREATE_TIME("createTime"),

    MODIFY_TIME("modifyTime");

    @Getter
    private String name;

}
