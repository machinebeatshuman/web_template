package web_template.dao;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.Validate;
import org.hibernate.Criteria;
import org.hibernate.ObjectNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import web_template.datamodel.schema.DatabaseField;
import web_template.datamodel.schema.Status;
import web_template.util.lang.BeanUtils;

/**
 * Don't support 'deleted' status now.
 * 
 * @author wangxiao
 */
@SuppressWarnings("unchecked")
public abstract class DaoImpl implements IDao {

    @Autowired
    protected SessionFactory sessionFactory;

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public <T> int getCount(Class<T> clazz, Date start) {
        Validate.notNull(clazz);
        Validate.notNull(start);

        Criteria criteria = getSession().createCriteria(clazz);
        if (start != null) {
            criteria.add(Restrictions.ge(DatabaseField.CREATE_TIME.getName(),
                    start));
        }
        return ((Number) criteria.setProjection(
                Projections.count(DatabaseField.ID.getName())).uniqueResult())
                .intValue();
    }

    public <T> T get(Class<T> clazz, Serializable id) {
        Validate.notNull(clazz);
        Validate.notNull(id);

        T result = (T) getSession().get(clazz, id);
        return result;
    }

    @Override
    public <T> List<T> getAll(Class<T> clazz) {
        Validate.notNull(clazz);

        return getSession().createCriteria(clazz).list();
    }

    @Override
    public <T> List<T> getListByStatus(Class<T> clazz, Status status) {
        Validate.notNull(clazz);

        Criteria criteria = getSession().createCriteria(clazz);
        if (status != null) {
            criteria.add(Restrictions.eq(DatabaseField.STATUS.getName(),
                    status.getValue()));
        }
        return criteria.addOrder(Order.desc(DatabaseField.ID.getName())).list();
    }

    @Override
    public <T> List<T> getListById(Class<T> clazz, Serializable id,
            boolean beforeOrAfter, int max) {
        Validate.notNull(clazz);

        Criterion criterion = beforeOrAfter ? Restrictions.le(
                DatabaseField.ID.getName(), id) : Restrictions.ge(
                DatabaseField.ID.getName(), id);
        return getSession()
                .createCriteria(clazz)
                .add(criterion)
                .add(Restrictions.eq(DatabaseField.STATUS.getName(), Status.OK))
                .addOrder(Order.desc(DatabaseField.ID.getName()))
                .setMaxResults(max).list();
    }

    @Override
    public <T extends Serializable> T save(Object object) {
        Validate.notNull(object);

        return (T) getSession().save(object);
    }

    @Override
    public void update(Object object) {
        Validate.notNull(object);

        getSession().update(object);
    }

    @Override
    public <T> void patch(T object) throws ObjectNotFoundException {
        Validate.notNull(object);

        // Gets ID.
        Class<T> clazz = (Class<T>) object.getClass();
        Serializable id;
        try {
            Field idField = clazz.getDeclaredField(DatabaseField.ID.getName());
            idField.setAccessible(true);
            id = (Serializable) idField.get(object);
        } catch (Exception e) {
            throw new RuntimeException("Failed to get object ID", e);
        }

        T old = get(clazz, id);
        if (old == null) {
            throw new ObjectNotFoundException(id, clazz.getCanonicalName());
        }

        BeanUtils.patchObject(object, old);
        update(old);
    }

    @Override
    public void saveOrUpdate(Object object) {
        Validate.notNull(object);

        getSession().saveOrUpdate(object);
    }

    @Override
    public void delete(Object object) {
        Validate.notNull(object);

        try {
            Field statusField = object.getClass().getDeclaredField(
                    DatabaseField.STATUS.getName());
            statusField.setAccessible(true);
            statusField.set(object, Status.DELETED);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public <T> void delete(Class<T> clazz, Serializable id) {
        Validate.notNull(clazz);
        Validate.notNull(id);

        delete(get(clazz, id));
    }

}
