package web_template.dao;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.ObjectNotFoundException;

import web_template.datamodel.schema.Status;

/**
 * If not specified explicitly, all parameters should not be null.
 * 
 * @author wangxiao
 */
public interface IDao {

    // ---------------------- count ----------------------- //

    /**
     * @param start
     *            included, null for all count
     */
    <T> int getCount(Class<T> clazz, Date start);

    // ---------------------- list ----------------------- //

    <T> List<T> getAll(Class<T> clazz);

    <T> List<T> getListByStatus(Class<T> clazz, Status status);

    /**
     * @param beforeOrAfter
     *            True before, false after. The given ID will be included.
     * @return A list of instances before or after specified ID, sorted in
     *         descending order.
     */
    <T> List<T> getListById(Class<T> clazz, Serializable id,
            boolean beforeOrAfter, int max);

    // ---------------------- object ----------------------- //

    <T> T get(Class<T> clazz, Serializable id);

    <T extends Serializable> T save(Object object);

    void update(Object object);

    /**
     * Like the HTTP PATCH, only update partial of the specified object.
     * 
     * @param object
     *            The object to patch, whose ID is required.
     * @throws ObjectNotFoundException
     *             If the object doesn't exist.
     */
    <T> void patch(T object) throws ObjectNotFoundException;

    void saveOrUpdate(Object object);

    /**
     * Soft deletion. Changes status to {@link Status#DELETED}.
     */
    void delete(Object object);

    <T> void delete(Class<T> clazz, Serializable id);

}
