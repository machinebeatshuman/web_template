package web_template.web.admin;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import web_template.util.LogUtils;
import web_template.util.web.AdminUtils;

/**
 * @author wangxiao
 */
@Controller
public class AdminController {

    @RequestMapping("/adminx")
    public String home() {
        return "admin/home";
    }

    /**
     * TODO: Encrypt login data.
     */
    @RequestMapping("/adminx/login")
    public String login(@RequestParam(required = false) String username,
            @RequestParam(required = false) String password,
            HttpServletRequest request) {
        if (username == null) {
            return "admin/login";
        }

        if (AdminUtils.logIn(request, username, password)) {
            LogUtils.ADMIN.info("login successfully: username=" + username);
            return home();
        } else { // error
            LogUtils.ADMIN.info("login failed: username=" + username
                    + ", password=" + password);
            return "admin/login";
        }
    }

    // ---------------------- API ------------------------ //

}
