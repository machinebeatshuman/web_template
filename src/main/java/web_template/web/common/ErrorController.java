package web_template.web.common;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import web_template.util.LogUtils;

/**
 * Show error pages.
 * 
 * @author wangxiao
 */
@Controller
public class ErrorController {

    @RequestMapping(value = "/error/{errorCode}")
    public String showError(@PathVariable int errorCode,
            HttpServletRequest request, Map<String, Object> map) {
        String referrer = request.getHeader("referer");
        LogUtils.STAT.info("Error page: errorCode=" + errorCode + ", referrer="
                + referrer);
        map.put("errorCode", errorCode);
        return "common/error";
    }

}
