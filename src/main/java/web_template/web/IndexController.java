package web_template.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wangxiao
 */
@Controller
public class IndexController {

    @RequestMapping({ "/", "/index" })
    public String index() {
        return "index";
    }

}
