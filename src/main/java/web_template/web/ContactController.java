package web_template.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author wangxiao
 */
@Controller
public class ContactController {

    @RequestMapping("/contact")
    public String showContact() {
        return "contact";
    }

}
