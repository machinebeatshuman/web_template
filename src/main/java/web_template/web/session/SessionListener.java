package web_template.web.session;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import lombok.extern.apachecommons.CommonsLog;

/**
 * @author wangxiao
 */
@CommonsLog
public class SessionListener implements HttpSessionListener {

	public static final AtomicInteger LOGINC_OUNT = new AtomicInteger(),
			ONLINE_COUNT = new AtomicInteger();

	public SessionListener() {
		new Timer(SessionListener.class.getSimpleName()).schedule(
				new TimerTask() {
					@Override
					public void run() {
						log.info("loginAfterRestart: " + LOGINC_OUNT
								+ ", online=" + ONLINE_COUNT);
					}
				}, 0, 5 * 60 * 1000);
	}

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		LOGINC_OUNT.incrementAndGet();
		ONLINE_COUNT.incrementAndGet();
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		ONLINE_COUNT.decrementAndGet();
	}
}
