package web_template.service.config;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CachedConfigTest {

    @Test
    public void testGetVariable() throws InterruptedException {
        CachedConfig config = new CachedConfig(new AutoIncreaseConfig());

        // hit cache
        config.setTtl(1000);
        assertEquals("0", config.getVariable("1"));
        assertEquals("0", config.getVariable("1"));

        // expire
        config.setTtl(0);
        assertEquals("1", config.getVariable("2"));
        Thread.sleep(10);
        assertEquals("2", config.getVariable("2"));
    }

    private static class AutoIncreaseConfig implements IConfig {

        private int value = 0;

        @Override
        public String getVariable(String name) {
            return String.valueOf(value++);
        }

        @Override
        public void update() {
        }

    }

}
