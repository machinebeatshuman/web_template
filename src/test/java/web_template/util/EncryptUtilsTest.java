package web_template.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import web_template.util.encrypt.EncryptUtils;

public class EncryptUtilsTest {

    @Test
    public void testAll() {
        String s = "123456";
        assertEquals(s, EncryptUtils.decrypt(EncryptUtils.encrypt(s)));
    }

}
