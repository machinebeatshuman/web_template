# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

A project template for building powerful web sites, with the following technologies:

* Front-end
    * jQuery: access DOM
    * Bootstrap: layout, navigator, components etc.
    * AngularJS/AngularUI: only in admin module currently
* Back-end
    * JavaSE 7
    * Spring MVC 3
    * Velocity
    * MySQL
* Management
    * Maven 3

### Setup for development ###

#### Database configuration ####

1. Install MySQL
   ```
   sudo yum install mysql
   sudo yum install mysql-server
   ```
1. Start and init MySQL
   ```
   sudo service mysqld start
   mysqladmin -u root password <password>
   ```
1. Add a user for development testing
   ```
   CREATE USER 'test'@'localhost' IDENTIFIED BY '123456';
   GRANT ALL ON *.* TO 'test'@'localhost';
   FLUSH PRIVILEGES;
   ```
1. Import schema from ```misc/db/web_template.sql```

#### Run web server ####

1. Replace all ```web_template``` (including directory name) with your project's name in the codes
1. Run jetty server in development model
   ```
   mvn org.mortbay.jetty:jetty-maven-plugin:run -P dev
   ```

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact